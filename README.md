# COSMO Explorer

The COSMO Explorer is a webapp developed as part of the German COVID-19 Snapshot Monitoring project (www.corona-monitor.de). It was designed to display behavioral data over time and can be used and adapted to other project's needs.

Note: This repository comes with some dummy data only. Please replace with your real data.